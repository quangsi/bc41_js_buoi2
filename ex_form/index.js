var usernameEl = document.getElementById("username");
// element

usernameEl.value = "alice";
// global scope
var passwordEl = document.getElementById("password");

function dangNhap() {
  // local scope
  var passwordValue = passwordEl.value;
  console.log("passwordValue", passwordValue);
  console.log("usernameValue", usernameEl.value);
}

// scope ~ phạm vi hoạt động

1 + 1;
1 + "1"; // "11"
